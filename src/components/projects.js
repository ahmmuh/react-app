import React, {Component } from 'react';
import {Tabs, Tab, Grid, Cell, Card, CardTitle, CardText, CardActions, Button, CardMenu, IconButton} from 'react-mdl';
class Projects extends Component{

    constructor(props){
        super(props);
        this.state = {activeTab: 0};
    }
    toggleCategories(){
        if (this.state.activeTab === 0){
            return(
    
<Card  shadow={0} style={{width: '400px', margin: 'auto'}}>
<CardTitle style={{color: '#fff', height: '300px', background: 'url(https://cdn-images-1.medium.com/max/1200/1*i3hzpSEiEEMTuWIYviYweQ.png) center / cover'}}>React .js</CardTitle>
    <CardText>
    Nu förtiden pratar man mycket om React, vad är React js?, det är ett effektivt Javascript bibliotek  och det avänds för att bygga hemsidor, enkelt och flexibelt.
    Det är språket som används i facebook, Instagram, Ubder och många stora företag.
    </CardText>
    <CardActions border>
        <Button colored>GitHub</Button>
        <Button colored>Bitbucket</Button>
    </CardActions>
    <CardMenu style={{color: '#fff'}}>
        <IconButton name="share" />
    </CardMenu>
</Card>
                
            )
        }else if(this.state.activeTab === 1) {
            return(

                   
<Card shadow={0} style={{width: '400px', margin: 'auto'}}>
<CardTitle style={{color: '#fff', height: '300px', background: 'url(https://christianliebel.com/wp-content/uploads/2016/02/Angular2-825x510.png) center / cover'}}>Angular</CardTitle>
    <CardText>
    Angular är ett ramverk som gör enkelt att bygga webb applikationer, Angular ger utvecklare möjlighet att bygga program för webb/skrivbord.
    </CardText>
    <CardActions border>
        <Button colored>GitHub</Button>
        <Button colored>Bitbucket</Button>
    </CardActions>
    <CardMenu style={{color: '#fff'}}>
        <IconButton name="share" />
    </CardMenu>
</Card>    

            )
        } else if (this.state.activeTab === 2){
            return(
         
<Card shadow={0} style={{width: '400px', margin: 'auto'}}>
<CardTitle style={{color: '#fff', height: '300px', background: 'url(https://miro.medium.com/max/788/1*K8-NHsRRBuUpuzphdkZ6MQ.png) center / cover'}}>ASP .NET Core</CardTitle>
    <CardText>
    ASP.NET Core är ett fritt och open-source webbramverk, det är enkelt att använda det, Jag gillar själv Code Firs och Database First. När man använder code first så bygger man bara sin modell sen kommer Entity FrameWork och ta hand om allt annat, t.e.x tabeller och databasen.
    </CardText>
    <CardActions border>
        <Button colored>GitHub</Button>
        <Button colored>Bitbucket</Button>
    </CardActions>
    <CardMenu style={{color: '#fff'}}>
        <IconButton name="share" />
    </CardMenu>
</Card>

            )

        } else if (this.state.activeTab === 3){
            return(
              
<Card shadow={0} style={{width: '400px', margin: 'auto'}}>
<CardTitle style={{color: '#fff', height: '300px', background: 'url(https://cdn-images-1.medium.com/max/1200/1*fnbqF0xNVwINs_RkygkX1g.png) center / cover'}}>React .js</CardTitle>
    <CardText>

kotlin är ett statiskt skrivet programmeringsspråk som körs på den virtuella Java-maskinen och kan också kompileras till JavaScript-källkod.
    </CardText>
    <CardActions border>
        <Button colored>GitHub</Button>
        <Button colored>Bitbucket</Button>
    </CardActions>
    <CardMenu style={{color: '#fff'}}>
        <IconButton name="share" />
    </CardMenu>
</Card>


            )
        } else if(this.state.activeTab === 4){
            return(
<Card class="animated infinite jello" shadow={0} style={{width: '400px', margin: 'auto'}}>
<CardTitle style={{color: '#fff', height: '300px', background: 'url(https://blog.tomasmahrik.com/wp-content/uploads/2015/06/swift.jpg) center / cover'}}>Build IOS Apps With Swift</CardTitle>
    <CardText>
    Swift är ett programmeringsspråk som utvecklats av Apple Inc. för iOS, macOS, watchOS, tvOS och Linux. Swift är utformat för att fungera med Apples kakao- och kakaohandelsramar och den stora delen av befintlig Objective-C-kod som skrivs för Apples produkter.
    </CardText>
    <CardActions border>
        <Button colored>GitHub</Button>
        <Button colored>Bitbucket</Button>
    </CardActions>
    <CardMenu style={{color: '#fff'}}>
        <IconButton name="share" />
    </CardMenu>
</Card>
            )
        }
        
    }

    render(){
        return(

            <div className="category-tabs">
            <Tabs activeTab={this.state.activeTab} onChange={(tabId) => this.setState({activeTab: tabId})} ripple>
            <Tab>React</Tab>
            <Tab>Angular</Tab>
            <Tab>.Net Core</Tab>
            <Tab>Kotlin</Tab>
            <Tab>Swift</Tab>
            </Tabs>

            <section className="projects-grid">
            <Grid className="projects-grid">
                <Cell col={12}>

                <div className="content">{this.toggleCategories()}</div>
                </Cell>
            </Grid>
            </section>
            </div>
        )
    }
}

export default Projects;