import React, {Component } from 'react';
import {Grid, Cell} from 'react-mdl';
class Home extends Component{
    render(){
        return(
            <div style={{width: '100%', margin: 'auto'}}>

            <Grid className="home-grid">
            <Cell col={12}>
            <img 
        src="https://scontent.fume1-1.fna.fbcdn.net/v/t1.0-9/37178540_1947303615320633_7514958707931742208_n.jpg?_nc_cat=108&_nc_ht=scontent.fume1-1.fna&oh=7223d6a2637701a4d574c35a4707baf3&oe=5C415EC7"
        alt="avatar"
        className="avatar-img"
        
        />

        <div className="banner-text">
        <h2>Web & App Developer</h2>
        <hr/>

        <p>HTML | BOOTSTRAP | CSS/LESS | JQUERY | REACT | REACT NATIVE | Angular | .NET CORE  </p>
        <div className="social">
      <a class="animated infinite fadeIn" href="https://bitbucket.org/dashboard/overview" rel="noopener noreferrer" target="_blank">
      <i className="fab fa-bitbucket"></i>

      </a>
      <a class="animated infinite jello" href="https://www.youtube.com/" rel="noopener noreferrer" target="_blank">
        <i className="fab fa-youtube-square"></i>

      </a>
      <a class="animated infinite flash" href="https://www.facebook.com/" rel="noopener noreferrer" target="_blank">
        <i className="fab fa-facebook-square"></i>

      </a>
      <a class="animated infinite pulse" href="https://github.com/ahmmuh" rel="noopener noreferrer" target="_blank">
        <i className="fab fa-github-square"></i>

      </a>
        </div>
            </div>
            </Cell>
                </Grid>


                
            </div>
            
        )
    }
}

export default Home;