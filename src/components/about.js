import React, {Component } from 'react';
import {Card, CardText, CardActions, CardTitle, Button} from 'react-mdl';
class About extends Component{
    render(){
        return(
<Card shadow={0} style={{width: '400px', height: '520px', marginLeft: 'auto', marginRight: 'auto', marginTop: '20px', border: '50%'}}>
    <CardTitle expand style={{color: '#fff', background: 'url(https://scontent.fume1-1.fna.fbcdn.net/v/t1.0-0/p206x206/35650149_1899861656731496_414553799642841088_n.jpg?_nc_cat=108&_nc_ht=scontent.fume1-1.fna&oh=7252a3200ae346eb167f9651fc934947&oe=5C866046) bottom right 15% no-repeat #46B6AC'}}>Om mig</CardTitle>
    <CardText>
    Jag heter Ahmed Mukhtar, har utbildat mig till IT tekniker på gymnasienivå men sen har jag bytt riktning till web/app utveckling. Jag fick ett intresse i mitt sista år på gymnasiet som gjorde mig att jag helt släppte ut hårdvaran ur mina händer och började fokusera på det som finns i hårdvaran t.ex. appar och webbsidor.
Men sedan har mitt intresse ökat mycket kraftigt och till slut bestämde jag mig att plugga till webb och apputvecklare. Nu håller jag på med LIA inom webben på Mobil Handel programmet och det går så bra för mig. Under praktiken har jag lärt mig Angular 2, Less, Asp.net core och mycket annat. Vill man lära sig något nytt så finns det möjligheten.
Under 2019 kommer jag gå på praktik inom apputveckling där kommer jag jobba mycket med båda IOS och Android appar. Förhoppningsvis kommer jag använda Swift inom IOS och Kotlin inom Android.

    </CardText>
    <CardActions border>
        <Button colored>Yeah</Button>
    </CardActions>
</Card>
        )

        
    }

    
}

export default About;