import React, {Component } from 'react';
import {List, ListItem, ListItemContent} from 'react-mdl';
class Contact extends Component{
    render(){
        return(
            <div className="contact">
       <List>
  <ListItem>
    <ListItemContent icon="person">Ahmed Mukhtar</ListItemContent>
  </ListItem>
  <ListItem>
    <ListItemContent icon="email">mukhtar1100@hotmail.com</ListItemContent>
  </ListItem>
  <ListItem>
    <ListItemContent icon="phone">+467 347 54 392</ListItemContent>
  </ListItem>
</List>
</div>
          
        )
    }
}

export default Contact;