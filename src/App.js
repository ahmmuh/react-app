import React, { Component } from 'react';
import './App.css';
import Main from './components/main';
import {Layout,Header, Navigation, Drawer,  Content} from 'react-mdl';
import { Link } from "react-router-dom";


class App extends Component {
  render() {
    return (
<div>
    <Layout fixedHeader>
        <Header className="header-color" title="Make Site" scroll>
            <Navigation className="navbar">
                <Link to="/home">Hem</Link>
                <Link to="/about">Om mig</Link>
                <Link to="/projects">Projekt</Link>
                <Link to="/contact">Kontakta mig</Link>
            </Navigation>
        </Header>
        <Drawer title="Make Site">
            <Navigation className="navbar">
            <Link to="/about">Om mig</Link>
                <Link to="/projects">Projekt</Link>
                <Link to="/contact">Kontakta mig</Link>
            </Navigation>
        </Drawer>

        <Main/>
        <Content />
    </Layout>
</div>
    );
  }
}

export default App;
